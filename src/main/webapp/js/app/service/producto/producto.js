demo.service.producto = (function() {

    function guardar(item) {
        var url = demo.service.url() + "productos/";
        return demo.service.post(url, item);
    }

    function eliminar(idItem) {
        var url = demo.service.url() + "productos/" + idItem;
        return demo.service.eliminar(url);
    }



    return {
        guardar: guardar,
        eliminar: eliminar
    };
})();