demo.ui.producto = (function () {

    var template;

    function init() {
        registrarEventos();
        obtenerTemplate().done(function (data) {
            template = data;
        });
    }

    function registrarEventos() {
        $('#lista').on('click', '.delete', function (event) {
            event.preventDefault();
            var $this = $(this);
            demo.service.producto.eliminar($this.data("id")).done(function () {
                $this.closest("li").slideUp(400);
            });
        });

        $('#alta').submit(function (event) {
            event.preventDefault();
            var item = {};
            item.nombre = $('#nombre').val();
            demo.service.producto.guardar(item).done(function (data) {
                var items = {"productos": [data], "upper": function () {
                        return function (text, render) {
                            return upper(render(text));
                        };
                    }};
                var html = Mustache.render(template, items);
                $('#lista').append(html);
                $("#alta")[0].reset();
            });
        });
    }


    function obtenerTemplate() {
        return $.ajax({
            url: "template/producto_partial.mustache"
        });
    }


    return {
        init: init
    };

})();
