package com.somospnt.mustache;

import java.io.FileReader;
import java.io.IOException;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.mustache.MustacheViewResolver;
import org.springframework.web.servlet.view.mustache.java.MustacheJTemplateFactory;

@Configuration
public class ViewInitializer {

    @Bean
    public ViewResolver getViewResolver(ResourceLoader resourceLoader) {
        MustacheViewResolver mustacheViewResolver = new MustacheViewResolver();
        mustacheViewResolver.setPrefix("/WEB-INF/views/");
        mustacheViewResolver.setSuffix(".mustache");
        mustacheViewResolver.setCache(false);
        mustacheViewResolver.setContentType("text/html;charset=utf-8");
        MustacheJTemplateFactory factory = new MustacheJTemplateFactory();
        factory.setResourceLoader(resourceLoader);
        mustacheViewResolver.setTemplateFactory(factory);
        return mustacheViewResolver;
    }

    @Bean
    public Invocable initJavaScript(ResourceLoader resourceLoader) throws ScriptException, IOException {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        engine.eval(new FileReader(resourceLoader.getResource("js/app/ui/template/templateFunctions.js").getFile()));
        return (Invocable) engine;
    }

}
