package com.somospnt.mustache.repository;

import com.somospnt.mustache.domain.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto, Long>{


}
