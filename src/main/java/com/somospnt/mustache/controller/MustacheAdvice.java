package com.somospnt.mustache.controller;

import com.google.common.base.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Invocable;
import javax.script.ScriptException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice(annotations = Controller.class)
public class MustacheAdvice {

    @Autowired
    private Invocable invocable;

    @ModelAttribute("upper")
    public Object upper() {
        return (Function<String, String>) (String aInput) -> {
            try {
                System.out.println("*********************** "+aInput);
                return (String) invocable.invokeFunction("upper", aInput);
            } catch (ScriptException | NoSuchMethodException ex) {
                Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        };

    }

}
