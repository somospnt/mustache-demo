package com.somospnt.mustache.controller;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.somospnt.mustache.repository.ProductoRepository;
import com.somospnt.mustache.domain.Producto;
import java.io.StringWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoRestController {

    @Autowired
    private ProductoRepository itemRepository;

    @RequestMapping(value = "api/productos/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        itemRepository.delete(id);
    }

    @RequestMapping(value = "api/productos", method = RequestMethod.POST)
    public Producto crear(@RequestBody Producto item) {
        itemRepository.save(item);
        return item;
    }

    @RequestMapping(value = "api/mail/{id}", method = RequestMethod.GET)
    public String mail(@PathVariable Long id) {
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile("templates/mail.mustache");
        return mustache.execute(new StringWriter(), itemRepository.findOne(id)).toString();
    }

}
