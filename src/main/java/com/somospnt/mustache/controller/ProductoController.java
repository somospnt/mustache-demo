package com.somospnt.mustache.controller;

import com.somospnt.mustache.repository.ProductoRepository;
import java.util.Locale;
import javax.script.ScriptException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProductoController {

    @Autowired
    private ProductoRepository itemRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Locale locale, Model model) throws ScriptException {
        model.addAttribute("productos", itemRepository.findAll());
        return "producto";
    }

}